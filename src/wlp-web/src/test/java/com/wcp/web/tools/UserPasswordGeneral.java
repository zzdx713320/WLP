package com.wcp.web.tools;

import com.farm.authority.password.PasswordProviderInter;
import com.farm.authority.password.PasswordProviderService;

public class UserPasswordGeneral {

	public static void main(String[] args) {
		// SIMPLE:简单类型,SAFE:安全类型
		{
			PasswordProviderInter passwordinter = PasswordProviderService.getInstanceProvider("SAFE");
			String dbPassword = passwordinter.getDBPasswordByPlaint("sysadmin", "111111");
			System.out.println("SAFE:"+dbPassword);
		}
		{
			PasswordProviderInter passwordinter = PasswordProviderService.getInstanceProvider("SIMPLE");
			String dbPassword = passwordinter.getDBPasswordByPlaint("sysadmin", "111111");
			System.out.println("SIMPLE:"+dbPassword);
		}
	}

}
